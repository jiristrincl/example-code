/* eslint-disable @typescript-eslint/no-explicit-any */
import { IGraphQLConnection } from '.';
import { container, MockFactory2TestResolvers } from '../../container';
import {
  IBannerCore, IBannerCoreSaved, IBannerCreate, IBannerSaved, IFileUrls,
} from '../entities';
import { IGraphQLRawRequestResponse } from '../identity/graphQLConnection';

import { IBannerGateway, BannerGateway } from './sales';

import { expect } from 'chai';

describe('Unit: BannerGateway', () => {
  let bannerCore: IBannerCore;
  let bannerCreate: IBannerCreate;
  let bannerSaved: IBannerSaved;
  let bannerCoreSaved: IBannerCoreSaved;
  let fileUrls: IFileUrls;

  let mockGraphQLConnection: IGraphQLConnection;
  let bannerGateway: IBannerGateway;
  let variableRunner: any;
  let init: () => void;

  before(() => {
    bannerCore = {
      name: 'lovely banner',
      isDraft: true,
      priority: 0,
      isDeleted: false,
    };
    bannerCreate = {
      ...bannerCore,
      authorId: 42,
      button: { isEnabled: false },
    };
    bannerSaved = {
      ...bannerCreate,
      id: 24,
    };
    bannerCoreSaved = {
      ...bannerCore,
      authorId: 42,
      id: 24,
    };
    fileUrls = {
      fileUrl: 'https://url',
      signedUrl: 'https://signedUrl',
      key: 'key',
    };

    mockGraphQLConnection = {
      sendUserRequest: async (): Promise<IGraphQLRawRequestResponse<any>> => ({} as IGraphQLRawRequestResponse),
      sendApiRequest: async (): Promise<IGraphQLRawRequestResponse<any>> => ({} as IGraphQLRawRequestResponse),
    };

    init = (): void => {
      bannerGateway = new BannerGateway(mockGraphQLConnection,
        MockFactory2TestResolvers.getInstance(container).mockContainer.utils.prometheus);
    };
  });

  it('.create()', async () => {
    mockGraphQLConnection = {
      ...mockGraphQLConnection,
      sendApiRequest: async (_operation: string, _queryOrMutation: string, variables: any): Promise<IGraphQLRawRequestResponse<any>> => {
        const result = {
          data: {
            bannerCreate: { banner: bannerSaved },
          },
          headers: {},
          status: 0,
        };
        variableRunner = variables;
        return result;
      },
    };
    init();

    const response = await bannerGateway.create(bannerCreate);
    expect(response).to.be.deep.equal(bannerSaved);
    expect(variableRunner).to.be.deep.equal({ banner: bannerCreate });
  });

  it('.update()', async () => {
    mockGraphQLConnection = {
      ...mockGraphQLConnection,
      sendApiRequest: async (_operation: string, _queryOrMutation: string, variables: any): Promise<IGraphQLRawRequestResponse<any>> => {
        const result = {
          data: {
            bannerUpdate: { banner: bannerSaved },
          },
          headers: {},
          status: 0,
        };
        variableRunner = variables;
        return result;
      },
    };
    init();

    const response = await bannerGateway.update({
      ...bannerCreate,
      id: 42,
    });
    expect(response).to.be.deep.equal(bannerSaved);
    expect(variableRunner).to.be.deep.equal({
      banner: {
        ...bannerCreate,
        id: 42,
      },
    });
  });

  it('.softUpdate()', async () => {
    mockGraphQLConnection = {
      ...mockGraphQLConnection,
      sendApiRequest: async (_operation: string, _queryOrMutation: string, variables: any): Promise<IGraphQLRawRequestResponse<any>> => {
        const result = {
          data: {
            bannerSoftUpdate: { banners: [bannerCoreSaved] },
          },
          headers: {},
          status: 0,
        };
        variableRunner = variables;
        return result;
      },
    };
    init();

    const response = await bannerGateway.softUpdate({ id: 4, priority: 1 });
    expect(response).to.be.deep.equal([bannerCoreSaved]);
    expect(variableRunner).to.be.deep.equal({
      banner: { id: 4, priority: 1 },
    });
  });

  it('.copy()', async () => {
    mockGraphQLConnection = {
      ...mockGraphQLConnection,
      sendApiRequest: async (_operation: string, _queryOrMutation: string, variables: any): Promise<IGraphQLRawRequestResponse<any>> => {
        const result = {
          data: {
            bannerCopy: { banner: bannerSaved },
          },
          headers: {},
          status: 0,
        };
        variableRunner = variables;
        return result;
      },
    };
    init();

    const response = await bannerGateway.copy(1, 42);
    expect(response).to.be.deep.equal(bannerSaved);
    expect(variableRunner).to.be.deep.equal({ id: 1, authorId: 42 });
  });

  it('.delete()', async () => {
    mockGraphQLConnection = {
      ...mockGraphQLConnection,
      sendApiRequest: async (_operation: string, _queryOrMutation: string, variables: any): Promise<IGraphQLRawRequestResponse<any>> => {
        const result = {
          data: {
            bannerDelete: { succeeded: true },
          },
          headers: {},
          status: 0,
        };
        variableRunner = variables;
        return result;
      },
    };
    init();

    const response = await bannerGateway.delete(42);
    expect(response).to.be.deep.equal(undefined);
    expect(variableRunner).to.be.deep.equal({
      id: 42,
    });
  });

  it('.getById()', async () => {
    mockGraphQLConnection = {
      ...mockGraphQLConnection,
      sendApiRequest: async (_operation: string, _queryOrMutation: string, variables: any): Promise<IGraphQLRawRequestResponse<any>> => {
        const result = {
          data: {
            bannerGetById: { banner: bannerSaved },
          },
          headers: {},
          status: 0,
        };
        variableRunner = variables;
        return result;
      },
    };
    init();

    const response = await bannerGateway.getById(42);
    expect(response).to.be.deep.equal(bannerSaved);
    expect(variableRunner).to.be.deep.equal({ id: 42 });
  });

  it('.getAll()', async () => {
    mockGraphQLConnection = {
      ...mockGraphQLConnection,
      sendApiRequest: async (_operation: string, _queryOrMutation: string, _variables: any): Promise<IGraphQLRawRequestResponse<any>> => {
        const result = {
          data: {
            bannerGetAll: { banners: [bannerCoreSaved] },
          },
          headers: {},
          status: 0,
        };
        return result;
      },
    };
    init();

    const response = await bannerGateway.getAll();
    expect(response).to.be.deep.equal([bannerCoreSaved]);
  });

  it('.getBySubtypes()', async () => {
    const subTypes: string[] = ['axia', 'prisma'];
    mockGraphQLConnection = {
      ...mockGraphQLConnection,
      sendApiRequest: async (_operation: string, _queryOrMutation: string, variables: any): Promise<IGraphQLRawRequestResponse<any>> => {
        const result = {
          data: {
            bannerGetBySubtypes: { banners: [bannerSaved] },
          },
          headers: {},
          status: 0,
        };
        variableRunner = variables;
        return result;
      },
    };
    init();

    const response = await bannerGateway.getBySubtypes(subTypes);
    expect(response).to.be.deep.equal([bannerSaved]);
    expect(variableRunner).to.be.deep.equal({ subTypes });
  });

  it('.getSignedUrl()', async () => {
    mockGraphQLConnection = {
      ...mockGraphQLConnection,
      sendApiRequest: async (_operation: string, _queryOrMutation: string, variables: any): Promise<IGraphQLRawRequestResponse<any>> => {
        const result = {
          data: {
            bannerGetSignedUrl: fileUrls,
          },
          headers: {},
          status: 0,
        };
        variableRunner = variables;
        return result;
      },
    };
    init();

    const response = await bannerGateway.getSignedUrl('jpg');
    expect(response).to.be.deep.equal(fileUrls);
    expect(variableRunner).to.be.deep.equal({ suffix: 'jpg' });
  });
});
