import {
  IBannerCoreSaved, IBannerCreate, IBannerSaved, IBannerSoftUpdate, IBannerUpdate, IFileUrls, IIdNumber,
} from '../../entities';
import { GraphQLQueryBuilder } from '../graphQLQueryBuilder';
import { IGraphQLConnection } from '../identity';

import { Prometheus } from 'tfs-npm-utils';

export interface IBannerGateway {
  create(banner: IBannerCreate): Promise<IBannerSaved>;
  update(banner: IBannerUpdate): Promise<IBannerSaved>;
  softUpdate(banner: IBannerSoftUpdate): Promise<IBannerCoreSaved[]>;
  copy(bannerId: number, authorId: number): Promise<IBannerSaved>;
  delete(id: number): Promise<void>;
  getAll(): Promise<IBannerCoreSaved[]>;
  getById(id: number): Promise<IBannerSaved>;
  getBySubtypes(subTypes: string[]): Promise<IBannerSaved[]>;
  getSignedUrl(suffix: string): Promise<IFileUrls>;
}

export class BannerGateway implements IBannerGateway {
  public constructor(
    private graphQLConnection: IGraphQLConnection,
    private prometheus: Prometheus,
  ) {}

  public create = async (banner: IBannerCreate): Promise<IBannerSaved> => {
    const operation = 'bannerCreate';
    this.prometheus.rest.outcoming.MessagesCount?.inc({ url: operation });
    const end = this.prometheus.rest.outcoming.MessagesTime?.startTimer({ url: operation });

    const query = GraphQLQueryBuilder.mutation<{ banner: IBannerCreate }, { banner: IBannerSaved }>({
      operation,
      variables: {
        banner: {
          required: true,
          value: banner,
          type: 'bannerCreateType',
        },
      },
      fields: [{
        banner: [
          'name', 'heading', 'content', 'isDraft',
          'priority', 'isDeleted', 'image',
          'durationFrom', 'durationTo',
          'subTypes', 'authorId', 'id', 'publicationDate',
          { button: ['isEnabled', 'link', 'text'] },
        ],
      }],
    });

    const response = await this.graphQLConnection.sendApiRequest<{
      bannerCreate: { banner: IBannerSaved };
    }>(operation, query.query, query.variables);
    end?.call(0);

    if (!response.data) {
      throw new Error('There was some problem with request');
    }

    return response.data.bannerCreate.banner;
  }

  public update = async (banner: IBannerUpdate): Promise<IBannerSaved> => {
    const operation = 'bannerUpdate';
    this.prometheus.rest.outcoming.MessagesCount?.inc({ url: operation });
    const end = this.prometheus.rest.outcoming.MessagesTime?.startTimer({ url: operation });

    const query = GraphQLQueryBuilder.mutation<{ banner: IBannerUpdate }, { banner: IBannerSaved }>({
      operation,
      variables: {
        banner: {
          required: true,
          value: banner,
          type: 'bannerUpdateType',
        },
      },
      fields: [{
        banner: [
          'name', 'heading', 'content', 'isDraft',
          'priority', 'isDeleted', 'image',
          'durationFrom', 'durationTo',
          'subTypes', 'authorId', 'id', 'publicationDate',
          { button: ['isEnabled', 'link', 'text'] },
        ],
      }],
    });

    const response = await this.graphQLConnection.sendApiRequest<{
      bannerUpdate: { banner: IBannerSaved };
    }>(operation, query.query, query.variables);
    end?.call(0);

    if (!response.data) {
      throw new Error('There was some problem with request');
    }

    return response.data.bannerUpdate.banner;
  }

  public softUpdate = async (banner: IBannerSoftUpdate): Promise<IBannerCoreSaved[]> => {
    const operation = 'bannerSoftUpdate';
    this.prometheus.rest.outcoming.MessagesCount?.inc({ url: operation });
    const end = this.prometheus.rest.outcoming.MessagesTime?.startTimer({ url: operation });

    const query = GraphQLQueryBuilder.mutation<{ banner: IBannerSoftUpdate }, { banners: IBannerCoreSaved[] }>({
      operation,
      variables: {
        banner: {
          required: true,
          value: banner,
          type: 'bannerSoftUpdateType',
        },
      },
      fields: [{
        banners: [
          'name', 'heading', 'content', 'isDraft',
          'priority', 'isDeleted', 'image',
          'durationFrom', 'durationTo',
          'authorId', 'id', 'publicationDate',
        ],
      }],
    });

    const response = await this.graphQLConnection.sendApiRequest<{
      bannerSoftUpdate: { banners: IBannerCoreSaved[] };
    }>(operation, query.query, query.variables);
    end?.call(0);

    if (!response.data) {
      throw new Error('There was some problem with request');
    }

    return response.data.bannerSoftUpdate.banners;
  }

  public copy = async (bannerId: number, authorId: number): Promise<IBannerSaved> => {
    const operation = 'bannerCopy';
    this.prometheus.rest.outcoming.MessagesCount?.inc({ url: operation });
    const end = this.prometheus.rest.outcoming.MessagesTime?.startTimer({ url: operation });

    const query = GraphQLQueryBuilder.mutation<{ id: number; authorId: number }, { banner: IBannerSaved }>({
      operation,
      variables: {
        id: {
          required: true,
          value: bannerId,
        },
        authorId: {
          required: true,
          value: authorId,
        },
      },
      fields: [{
        banner: [
          'name', 'heading', 'content', 'isDraft',
          'priority', 'isDeleted', 'image',
          'durationFrom', 'durationTo',
          'subTypes', 'authorId', 'id', 'publicationDate',
          { button: ['isEnabled', 'link', 'text'] },
        ],
      }],
    });

    const response = await this.graphQLConnection.sendApiRequest<{
      bannerCopy: { banner: IBannerSaved };
    }>(operation, query.query, query.variables);
    end?.call(0);

    if (!response.data) {
      throw new Error('There was some problem with request');
    }

    return response.data.bannerCopy.banner;
  }

  public delete = async (id: number): Promise<void> => {
    const operation = 'bannerDelete';
    this.prometheus.rest.outcoming.MessagesCount?.inc({ url: operation });
    const end = this.prometheus.rest.outcoming.MessagesTime?.startTimer({ url: operation });

    const query = GraphQLQueryBuilder.mutation<IIdNumber, { succeeded: boolean }>({
      operation,
      variables: {
        id: { value: id, required: true },
      },
      fields: ['succeeded'],
    });

    const response = await this.graphQLConnection.sendApiRequest<{
      bannerDelete: { succeeded: boolean };
    }>(operation, query.query, query.variables);
    end?.call(0);

    if (!response.data) {
      throw new Error('There was some problem with request');
    }
  }

  public getAll = async (): Promise<IBannerCoreSaved[]> => {
    const operation = 'bannerGetAll';
    this.prometheus.rest.outcoming.MessagesCount?.inc({ url: operation });
    const end = this.prometheus.rest.outcoming.MessagesTime?.startTimer({ url: operation });

    const query = GraphQLQueryBuilder.query<undefined, { banners: IBannerCoreSaved[] }>({
      operation,
      fields: [{
        banners: [
          'name', 'heading', 'content', 'isDraft',
          'priority', 'isDeleted', 'image',
          'durationFrom', 'durationTo',
          'authorId', 'id', 'publicationDate',
        ],
      }],
    });

    const response = await this.graphQLConnection.sendApiRequest<{
      bannerGetAll: { banners: IBannerCoreSaved[] };
    }>(operation, query.query, query.variables);
    end?.call(0);

    if (!response.data) {
      throw new Error('There was some problem with request');
    }

    return response.data.bannerGetAll.banners;
  }

  public getById = async (id: number): Promise<IBannerSaved> => {
    const operation = 'bannerGetById';
    this.prometheus.rest.outcoming.MessagesCount?.inc({ url: operation });
    const end = this.prometheus.rest.outcoming.MessagesTime?.startTimer({ url: operation });

    const query = GraphQLQueryBuilder.query<IIdNumber, { banner: IBannerSaved }>({
      operation,
      variables: {
        id: {
          required: true,
          value: id,
          type: 'Int',
        },
      },
      fields: [{
        banner: [
          'name', 'heading', 'content', 'isDraft',
          'priority', 'isDeleted', 'image',
          'durationFrom', 'durationTo',
          'subTypes', 'authorId', 'id', 'publicationDate',
          { button: ['isEnabled', 'link', 'text'] },
        ],
      }],
    });

    const response = await this.graphQLConnection.sendApiRequest<{
      bannerGetById: { banner: IBannerSaved };
    }>(operation, query.query, query.variables);
    end?.call(0);

    if (!response.data) {
      throw new Error('There was some problem with request');
    }

    return response.data.bannerGetById.banner;
  }

  public getBySubtypes = async (subTypes: string[]): Promise<IBannerSaved[]> => {
    const operation = 'bannerGetBySubtypes';
    this.prometheus.rest.outcoming.MessagesCount?.inc({ url: operation });
    const end = this.prometheus.rest.outcoming.MessagesTime?.startTimer({ url: operation });

    const query = GraphQLQueryBuilder.query<{ subTypes: string[] }, { banners: IBannerSaved[] }>({
      operation,
      variables: {
        subTypes: {
          required: true,
          value: subTypes,
          list: true,
        },
      },
      fields: [{
        banners: [
          'id', 'name', 'heading', 'content', 'isDraft',
          'priority', 'isDeleted', 'authorId', 'image',
          'publicationDate', 'durationFrom', 'durationTo',
          { button: ['isEnabled', 'link', 'text'] },
        ],
      }],
    });

    const response = await this.graphQLConnection.sendApiRequest<{
      bannerGetBySubtypes: { banners: IBannerSaved[] };
    }>(operation, query.query, query.variables);
    end?.call(0);

    if (!response.data) {
      throw new Error('There was some problem with request');
    }

    return response.data.bannerGetBySubtypes.banners;
  }

  public getSignedUrl = async (suffix: string): Promise<IFileUrls> => {
    const operation = 'bannerGetSignedUrl';
    this.prometheus.rest.outcoming.MessagesCount?.inc({ url: operation });
    const end = this.prometheus.rest.outcoming.MessagesTime?.startTimer({ url: operation });

    const query = GraphQLQueryBuilder.query<{suffix: string}, IFileUrls>({
      operation,
      variables: {
        suffix: {
          required: true,
          value: suffix,
        },
      },
      fields: ['key', 'signedUrl', 'fileUrl', { headers: ['contentDisposition'] }],
    });

    const response = await this.graphQLConnection.sendApiRequest<{
      bannerGetSignedUrl: IFileUrls;
    }>(operation, query.query, query.variables);
    end?.call(0);

    if (!response.data) {
      throw new Error('There was some problem with request');
    }

    return response.data.bannerGetSignedUrl;
  }
}
