
export interface IBannerCore {
  name: string;
  heading?: string;
  content?: string;
  isDraft: boolean;
  priority: number;
  isDeleted: boolean;
  image?: string;
  durationFrom?: string;
  durationTo?: string;
  subTypes?: string[];
}

export interface IBannerSoftUpdate {
  id: number;
  isDraft?: boolean;
  priority?: number;
  isDeleted?: boolean;
}

export interface IBannerButton {
  isEnabled: boolean;
  text?: string;
  link?: string;
}

export interface IBannerCreate extends IBannerCore {
  authorId: number;
  button: IBannerButton;
}

export interface IBannerUpdate extends IBannerCreate {
  id: number;
}

export interface IBannerCoreSaved extends IBannerCore {
  id: number;
  publicationDate?: string;
  authorId: number;
}

export interface IBannerCoreFull extends IBannerCoreSaved {
  authorName: string;
  authorAvatar?: string;
}

export interface IBannerSaved extends IBannerCoreSaved {
  button: IBannerButton;
}

export interface IBannerFull extends IBannerSaved {
  authorName: string;
  authorAvatar?: string;
}

