import { IBannerSaved } from '../interfaces';

import { bannerButtonType } from './bannerFullType';

import {
  GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLNonNull, GraphQLBoolean,
} from 'graphql';

export const bannerWithButtonType = new GraphQLObjectType<IBannerSaved>({
  name: 'bannerWithButtonType',
  description: 'bannerWithButton type',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    heading: {
      type: GraphQLString,
    },
    content: {
      type: GraphQLString,
    },
    isDraft: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    priority: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    isDeleted: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    authorId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    image: {
      type: GraphQLString,
    },
    publicationDate: {
      type: GraphQLString,
    },
    durationFrom: {
      type: GraphQLString,
    },
    durationTo: {
      type: GraphQLString,
    },
    button: {
      type: new GraphQLNonNull(bannerButtonType),
    },
  },
});
