import { MockFactory2TestResolvers, container } from '../../../../container';

import { IGroupEmpty, IInstrumentFull } from '../../../../entities';

import { expect } from 'chai';
import * as httpCodes from 'http-codes';
import * as request from 'supertest';

describe('functional: bannerGetForUser', () => {
  let mockFactory: MockFactory2TestResolvers;
  let query: string;
  let instrument: IInstrumentFull;
  let groupEmptyDb: IGroupEmpty;

  before(() => {
    mockFactory = MockFactory2TestResolvers.getInstance(container);
    query = `
      query bannerGetForUser {
        bannerGetForUser {
          banners {
            id
            name
            heading
            content
            isDraft
            priority
            isDeleted
            authorId
            image
            publicationDate
            durationFrom
            durationTo
            button {
              isEnabled
              text
              link
            }
          }
        }
      }
      `;
  });

  beforeEach(async () => {
    await mockFactory.mockContainer.utils.dbTestCleaner.cleanAll();
    await mockFactory.createCompany();

    instrument = await mockFactory.mockContainer.services.instrumentFull.create(mockFactory.user, mockFactory.instrument);

    groupEmptyDb = await container.gateways.group.save({
      companyId: mockFactory.company.id,
      description: 'desc',
      instruments: [],
      name: 'name',
      users: [],
    });

    await container.gateways.groupUser.save(groupEmptyDb.id, mockFactory.user.id!);
    await container.gateways.groupInstrument.save(groupEmptyDb.id, instrument.id);
  });

  it('bannerGetForUser', async () => {
    const response = await request(mockFactory.expressServer.getServer())
      .post(mockFactory.graphqlPath)
      .send({ query })
      .set({ Authorization: mockFactory.tokenLogin })
      .expect(httpCodes.OK);

    expect(response.body.data.bannerGetForUser.banners[0].name).to.be.deep.equal('my banner');
    expect(response.body.data.bannerGetForUser.banners[0].button.text).to.be.deep.equal('click!!');
  });
});
