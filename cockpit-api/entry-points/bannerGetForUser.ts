import {
  IBannerSaved,
  bannerWithButtonType,
} from '../entities';
import { IContext, IResolver, ISource, IAuthorizationComponent } from '../../../../infrastructure/apolloServer';

import {
  GraphQLList, GraphQLNonNull, GraphQLObjectType, GraphQLResolveInfo,
} from 'graphql';
import { BadRequestError } from 'tfs-npm-utils';

export class BannerGetForUserResolver implements IResolver<unknown, { banners: IBannerSaved[] }> {
  public name = 'bannerGetForUser';
  public description = 'get banners for user';

  public type = new GraphQLNonNull(new GraphQLObjectType<{ banners: IBannerSaved[] }>({
    name: 'envelopedBannersForUser',
    description: 'return of bannerGetForUser resolver',
    fields: {
      banners: {
        type: new GraphQLNonNull(new GraphQLList(bannerWithButtonType)),
      },
    },
  }));

  public resolve = this.authorization.check<unknown>(
    this.authorization.security.permission.readOwnBanner, // user
    async (
      _source: ISource,
      _args: unknown,
      context: IContext,
      _info: GraphQLResolveInfo,
    ): Promise<{ banners: IBannerSaved[] } | Error> => {
      try {
        if (!context.user || !context.user.id) {
          throw new BadRequestError('User not authenticated');
        }

        const bannerResponse = await context.bannerService.getForUser(context.user);

        return { banners: bannerResponse };
      } catch (e) {
        return context.errorHandling(e);
      }
    },
  );

  public constructor(private authorization: IAuthorizationComponent) {}
}
