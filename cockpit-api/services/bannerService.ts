import { IUserGateway, IBannerGateway } from '../../data-providers';
import {
  IBannerButton,
  IBannerCore,
  IBannerCoreFull,
  IBannerCoreSaved,
  IBannerFull,
  IBannerSaved,
  IBannerSoftUpdate,
  IBannerUpdate,
  IFileUrls,
  IInstrumentWithUsername,
  IUser, IUserWithId,
} from '../entities';

import { IInstrumentService } from '../instrumentService';

import { Prometheus, Logger } from 'tfs-npm-utils';

export interface IBannerService {
  create(banner: IBannerCore, button: IBannerButton, author: IUserWithId): Promise<IBannerFull>;
  update(banner: IBannerUpdate): Promise<IBannerFull>;
  softUpdate(bannerUpdate: IBannerSoftUpdate): Promise<IBannerCoreFull[]>;
  copy(bannerId: number, author: IUserWithId): Promise<IBannerFull>;
  delete(id: number): Promise<void>;
  getById(id: number): Promise<IBannerFull>;
  getForUser(user: IUserWithId): Promise<IBannerSaved[]>;
  getAll(): Promise<IBannerCoreFull[]>;
  getSignedUrl(suffix: string): Promise<IFileUrls>;
}

enum MethodsList {
  create = 'create',
  update = 'update',
  softUpdate = 'softUpdate',
  copy = 'copy',
  delete = 'delete',
  getById = 'getById',
  getForUser = 'getForUser',
  getAll = 'getAll',
  getSignedUrl = 'getSignedUrl',
}

export class BannerService implements IBannerService {
  public constructor(
    private bannerGateway: IBannerGateway,
    private userGateway: IUserGateway,
    private instrumentService: IInstrumentService,
    private logger: Logger,
    private prometheus: Prometheus,
  ) {}

  public create = async (banner: IBannerCore, button: IBannerButton, author: IUserWithId): Promise<IBannerFull> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.create });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.create });

    const bannerSaved = await this.bannerGateway.create({
      ...banner,
      authorId: author.id,
      button,
    });

    const bannerFull: IBannerFull = {
      ...bannerSaved,
      authorName: author.username,
      authorAvatar: author.avatar || undefined,
    };

    this.logger.trace('New banner was created', { bannerFull });

    end?.call(0);
    return bannerFull;
  }

  public update = async (banner: IBannerUpdate): Promise<IBannerFull> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.update });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.update });

    const bannerSaved = await this.bannerGateway.update(banner);

    const [author] = await this.userGateway.getByIdArray([bannerSaved.authorId], true);

    const bannerFull: IBannerFull = {
      ...bannerSaved,
      authorName: author.username,
      authorAvatar: author.avatar || undefined,
    };

    end?.call(0);
    return bannerFull;
  }

  public softUpdate = async (bannerUpdate: IBannerSoftUpdate): Promise<IBannerCoreFull[]> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.softUpdate });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.softUpdate });

    const banners = await this.bannerGateway.softUpdate(bannerUpdate);

    const bannersFull = await this.mapAuthorsToBanners(banners);

    end?.call(0);
    return bannersFull;
  }

  public copy = async (bannerId: number, author: IUserWithId): Promise<IBannerFull> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.copy });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.copy });

    const bannerSaved = await this.bannerGateway.copy(bannerId, author.id);

    const bannerFull: IBannerFull = {
      ...bannerSaved,
      authorName: author.username,
      authorAvatar: author.avatar || undefined,
    };

    end?.call(0);
    return bannerFull;
  }

  public delete = async (id: number): Promise<void> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.delete });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.delete });

    await this.bannerGateway.delete(id);

    end?.call(0);
  }

  public getById = async (id: number): Promise<IBannerFull> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.getById });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.getById });

    const bannerSaved = await this.bannerGateway.getById(id);

    const [author] = await this.userGateway.getByIdArray([bannerSaved.authorId], true);

    const bannerFull: IBannerFull = {
      ...bannerSaved,
      authorName: author.username,
      authorAvatar: author.avatar || undefined,
    };

    end?.call(0);
    return bannerFull;
  }

  public getForUser = async (user: IUserWithId): Promise<IBannerSaved[]> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.getForUser });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.getForUser });

    const myInstruments = await this.instrumentService.getAllAccessible(user);

    const subTypes: string[] = myInstruments.map((instrument: IInstrumentWithUsername) => instrument.subType);

    const uniqueSubTypes: string[] = subTypes.filter(
      (
        subType: string,
        index: number,
        array: string[],
      ) => array.indexOf(subType) === index,
    );

    const bannersWithButton = await this.bannerGateway.getBySubtypes(uniqueSubTypes);

    end?.call(0);
    return bannersWithButton;
  }

  public getAll = async (): Promise<IBannerCoreFull[]> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.getAll });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.getAll });

    const bannersSaved = await this.bannerGateway.getAll();

    const bannersFull = await this.mapAuthorsToBanners(bannersSaved);

    end?.call(0);
    return bannersFull;
  }

  public getSignedUrl = async (suffix: string): Promise<IFileUrls> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.getSignedUrl });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.getSignedUrl });

    const urls = await this.bannerGateway.getSignedUrl(suffix);
    end?.call(0);
    return urls;
  };

  private mapAuthorsToBanners = async (banners: IBannerCoreSaved[]): Promise<IBannerCoreFull[]> => {
    const authorsId: number[] = banners.map((banner: IBannerCoreSaved) => banner.authorId);

    const uniqueAuthorIds: number[] = authorsId.filter(
      (
        id: number,
        index: number,
        array: number[],
      ) => array.indexOf(id) === index,
    );

    const authors = await this.userGateway.getByIdArray(uniqueAuthorIds, true);

    return banners.map((banner: IBannerCoreSaved) => {
      const [author] = authors.filter((user: IUser) => user.id === banner.authorId);
      return {
        ...banner,
        authorName: author.username,
        authorAvatar: author.avatar || undefined,
      };
    });
  }
}
