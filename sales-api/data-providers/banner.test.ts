import { container, MockFactory2TestResolvers } from '../../../container';
import { IBannerCore } from '../../../entities';

import { IBannerGateway } from '.';

import { expect } from 'chai';

describe('acceptance: bannerGateway', () => {
  let banner: IBannerCore;
  let bannerGateway: IBannerGateway;
  let mockFactory: MockFactory2TestResolvers;

  before(async () => {
    mockFactory = MockFactory2TestResolvers.getInstance(container);
    await container.utils.dbCleaner.cleanAll();
    banner = mockFactory.banner;

    bannerGateway = container.gateways.banner;
  });

  it('.save(), .getById()', async () => {
    const response = await bannerGateway.save(banner);

    expect(response).to.be.deep.equal({
      ...banner,
      id: response?.id,
    });
  });

  it('.update()', async () => {
    const saved = await bannerGateway.save(banner);

    const response = await bannerGateway.update({
      ...banner,
      id: saved!.id,
      heading: 'updated heading',
    });

    expect(response).to.be.deep.equal({
      ...saved,
      heading: 'updated heading',
    });
  });

  it('.softUpdate()', async () => {
    const saved = await bannerGateway.save(banner);

    const response = await bannerGateway.softUpdate({
      id: saved!.id,
      priority: 2,
    });

    expect(response).to.be.deep.equal({
      ...saved,
      priority: 2,
    });
  });

  it('.delete()', async () => {
    const saved = await bannerGateway.save(banner);

    await bannerGateway.delete(saved!.id);
    const response = await bannerGateway.getById(saved!.id);

    expect(response).to.be.deep.equal(undefined);
  });

  it('.softDelete()', async () => {
    const saved = await bannerGateway.save(banner);

    const response = await bannerGateway.softDelete(saved!.id);

    expect(response).to.be.deep.equal({
      ...saved,
      isDeleted: true,
      isDraft: true,
      publicationDate: undefined,
    });
  });

  it('.getAll()', async () => {
    const saved = await bannerGateway.save(banner);
    const response = await bannerGateway.getAl);

    expect(response).to.be.deep.equal([{
      ...banner,
      id: saved?.id,
    }]);
  });

  it('.getByIdArray()', async () => {
    const saved1 = await bannerGateway.save(banner);
    const saved2 = await bannerGateway.save(banner);
    const saved3 = await bannerGateway.save({
      ...banner,
      isDraft: true,
    });
    const response = await bannerGateway.getByIdArray([saved1!.id, saved2!.id, saved3!.id]);

    expect(response.length).to.be.deep.equal(2);
  });

  it('.getByIdArray() - sorted', async () => {
    const saved1 = await bannerGateway.save(banner);
    const saved2 = await bannerGateway.save({
      ...banner,
      priority: 1,
    });
    const saved3 = await bannerGateway.save({
      ...banner,
    });
    const response = await bannerGateway.getByIdArray([saved1!.id, saved2!.id, saved3!.id]);

    expect(response.length).to.be.deep.equal(3);
    expect(response[0].id).to.be.deep.equal(saved2?.id);
  });

  it('.getByIdArray() - only active = false', async () => {
    const saved1 = await bannerGateway.save(banner);
    const saved2 = await bannerGateway.save(banner);
    const saved3 = await bannerGateway.save({
      ...banner,
      isDraft: true,
    });
    const response = await bannerGateway.getByIdArray([saved1!.id, saved2!.id, saved3!.id], false);

    expect(response.length).to.be.deep.equal(3);
  });
});
