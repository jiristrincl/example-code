import {
  IAjvSchemas,
  IBannerCore, IBannerSnakeCase, IBannerSoftUpdate, IBannerWithId,
} from '../../../entities';

import { IKnexConnection, IValidatorConstructor, Prometheus } from 'tfs-npm-utils';

export interface IBannerGateway {
  save(banner: IBannerCore): Promise< IBannerWithId | undefined>;
  update(banner: IBannerWithId): Promise< IBannerWithId | undefined>;
  softUpdate(banner: IBannerSoftUpdate): Promise<IBannerWithId | undefined>;
  delete(id: number): Promise<void>;
  softDelete(bannerId: number): Promise<IBannerWithId | undefined>;
  getById(id: number, isDeleted?: boolean): Promise<IBannerWithId | undefined>;
  getAll(): Promise<IBannerWithId[]>;
  getByIdArray(ids: number[], onlyActive?: boolean, isDeleted?: boolean, sorted?: boolean): Promise< IBannerWithId[]>;
}

export class BannerGateway implements IBannerGateway {
  public constructor(
    private readonly tableName: string,
    private connection: IKnexConnection,
    private prometheus: Prometheus,
    private validator: IValidatorConstructor,
    private readonly ajvSchemas: IAjvSchemas,
  ) {}

  public save = async (banner: IBannerCore): Promise<IBannerWithId | undefined> => {
    this.prometheus.database.MessagesCount?.inc({ tableName: this.tableName });
    const end = this.prometheus.database.MessagesTime?.startTimer({
      tableName: this.tableName,
    });

    const bannerForDd = {
      name: banner.name,
      heading: banner.heading,
      content: banner.content,
      is_draft: banner.isDraft,
      priority: banner.priority,
      is_deleted: banner.isDeleted,
      author_id: banner.authorId,
      image: banner.image,
      publication_date: banner.publicationDate ? new Date(banner.publicationDate) : undefined,
      duration_from: banner.durationFrom ? new Date(banner.durationFrom) : undefined,
      duration_to: banner.durationTo ? new Date(banner.durationTo) : new Date('9999-12-31'),
    };

    const [id] = await this.connection(this.tableName)
      .insert(bannerForDd)
      .transacting();

    this.validator.validateData(this.ajvSchemas.id, id);

    const savedBanner: IBannerWithId | undefined = await this.getById(id);

    end?.call(0);
    return savedBanner;
  };

  public update = async (banner: IBannerWithId): Promise<IBannerWithId | undefined> => {
    this.prometheus.database.MessagesCount?.inc({ tableName: this.tableName });
    const end = this.prometheus.database.MessagesTime?.startTimer({
      tableName: this.tableName,
    });
    const bannerForDd = {
      name: banner.name,
      heading: banner.heading || null,
      content: banner.content || null,
      is_draft: banner.isDraft,
      priority: banner.priority,
      is_deleted: banner.isDeleted,
      image: banner.image || null,
      publication_date: banner.publicationDate ? new Date(banner.publicationDate) : null,
      duration_from: banner.durationFrom ? new Date(banner.durationFrom) : null,
      duration_to: banner.durationTo ? new Date(banner.durationTo) : new Date('9999-12-31'),
    };

    await this.connection(this.tableName)
      .where('id', banner.id)
      .update(bannerForDd)
      .transacting();

    const updatedBanner: IBannerWithId | undefined = await this.getById(banner.id);

    end?.call(0);
    return updatedBanner;
  };

  public softUpdate = async (banner: IBannerSoftUpdate): Promise<IBannerWithId | undefined> => {
    this.prometheus.database.MessagesCount?.inc({ tableName: this.tableName });
    const end = this.prometheus.database.MessagesTime?.startTimer({
      tableName: this.tableName,
    });
    let publicationDate: Date | undefined | null;
    if (banner.publicationDate === null) {
      publicationDate = null;
    } else {
      publicationDate = banner.publicationDate ? new Date(banner.publicationDate) : undefined;
    }

    const bannerForDd = {
      is_draft: banner.isDraft,
      priority: banner.priority,
      is_deleted: banner.isDeleted,
      publication_date: publicationDate,
    };

    const id = await this.connection(this.tableName)
      .where('id', banner.id)
      .update(bannerForDd)
      .transacting();

    this.validator.validateData(this.ajvSchemas.id, id);

    const updatedBanner: IBannerWithId | undefined = await this.getById(banner.id);

    end?.call(0);
    return updatedBanner;
  };

  public delete = async (id: number): Promise<void> => {
    this.prometheus.database.MessagesCount?.inc({ tableName: this.tableName });
    const end = this.prometheus.database.MessagesTime?.startTimer({
      tableName: this.tableName,
    });

    await this.connection(this.tableName)
      .where('id', id)
      .delete()
      .transacting();

    end?.call(0);
  };

  public softDelete = async (bannerId: number): Promise<IBannerWithId | undefined> => {
    this.prometheus.database.MessagesCount?.inc({ tableName: this.tableName });
    const end = this.prometheus.database.MessagesTime?.startTimer({
      tableName: this.tableName,
    });

    const bannerForDd = {
      is_draft: true,
      publication_date: null,
      is_deleted: true,
    };

    const id = await this.connection(this.tableName)
      .where('id', bannerId)
      .update(bannerForDd)
      .transacting();

    this.validator.validateData(this.ajvSchemas.id, id);

    const updatedBanner: IBannerWithId | undefined = await this.getById(bannerId, true);

    end?.call(0);
    return updatedBanner;
  }

  public getById = async (id: number, isDeleted: boolean = false): Promise<IBannerWithId | undefined> => {
    this.prometheus.database.MessagesCount?.inc({ tableName: this.tableName });
    const end = this.prometheus.database.MessagesTime?.startTimer({
      tableName: this.tableName,
    });

    let [bannerFromDb] = await this.connection(this.tableName)
      .select()
      .where({
        id,
        is_deleted: isDeleted,
      })
      .transacting();

    if (!bannerFromDb) {
      end?.call(0);
      return undefined;
    }

    bannerFromDb = this.mapForValidation(bannerFromDb);
    this.validator.validateData(this.ajvSchemas.banner, bannerFromDb);
    const banner: IBannerWithId = this.mapBanner(bannerFromDb);

    end?.call(0);
    return banner;
  };

  public getAll = async (): Promise<IBannerWithId[]> => {
    this.prometheus.database.MessagesCount?.inc({ tableName: this.tableName });
    const end = this.prometheus.database.MessagesTime?.startTimer({
      tableName: this.tableName,
    });

    let bannersFromDb = await this.connection(this.tableName)
      .select()
      .transacting();

    if (!bannersFromDb.length) {
      end?.call(0);
      return [];
    }

    bannersFromDb = bannersFromDb.map((bannerFromDb: any) => this.mapForValidation(bannerFromDb));
    this.validator.validateCollection(this.ajvSchemas.banner, bannersFromDb);
    const banners: IBannerWithId[] = bannersFromDb.map((banner: IBannerSnakeCase) => this.mapBanner(banner));

    end?.call(0);
    return banners;
  };

  public getByIdArray = async (
    ids: number[],
,
    onlyActive: boolean = true,
    isDeleted: boolean = false,
    sorted: boolean = true,
  ): Promise< IBannerWithId[]> => {
    this.prometheus.database.MessagesCount?.inc({ tableName: this.tableName });
    const end = this.prometheus.database.MessagesTime?.startTimer({
      tableName: this.tableName,
    });

    let query = this.connection(this.tableName)
      .select()
      .whereIn('id', ids)
      .where({
        is_deleted: isDeleted,
      });

    if (onlyActive) {
      query = query.where({ is_draft: false })
        .where('duration_from', '<', new Date())
        .where('duration_to', '>', new Date());
    }

    if (sorted) {
      query = query.orderBy([
        { column: 'priority', order: 'desc' },
        { column: 'created_at', order: 'desc' },
      ]);
    }

    let bannersFromDb = await query
      .transacting();

    if (!bannersFromDb.length) {
      end?.call(0);
      return [];
    }

    bannersFromDb = bannersFromDb.map((bannerFromDb: any) => this.mapForValidation(bannerFromDb));
    this.validator.validateCollection(this.ajvSchemas.banner, bannersFromDb);
    const banners: IBannerWithId[] = bannersFromDb.map((banner: IBannerSnakeCase) => this.mapBanner(banner));

    end?.call(0);
    return banners;
  };

  private mapForValidation = (banner: any): IBannerSnakeCase => ({
    ...banner,
    publication_date: banner.publication_date ? banner.publication_date.toISOString() : undefined,
    duration_from: banner.duration_from ? banner.duration_from.toISOString() : undefined,
    duration_to: banner.duration_to ? banner.duration_to.toISOString() : undefined,
  });

  private mapBanner = (bannerFromDb: IBannerSnakeCase): IBannerWithId => {
    let durationTo: string | undefined = bannerFromDb.duration_to || undefined;
    if (
      bannerFromDb.duration_to
      && bannerFromDb.duration_to.startsWith('9999')
    ) {
      durationTo = undefined;
    }

    return {
      id: bannerFromDb.id,
      name: bannerFromDb.name,
      heading: bannerFromDb.heading || undefined,
      content: bannerFromDb.content || undefined,
      isDraft: !!bannerFromDb.is_draft,
      priority: bannerFromDb.priority,
      isDeleted: !!bannerFromDb.is_deleted,
      authorId: bannerFromDb.author_id,
      image: bannerFromDb.image || undefined,
      publicationDate: bannerFromDb.publication_date || undefined,
      durationFrom: bannerFromDb.duration_from || undefined,
      durationTo,
    };
  };
}

