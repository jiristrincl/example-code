import { IBannerFull, bannerFullEnvelopedType } from '../entities';
import { IContext, IResolver, ISource, IAuthorizationComponent } from '../../../../infrastructure/apolloServer';

import { GraphQLInt, GraphQLNonNull, GraphQLResolveInfo } from 'graphql';

export interface IBannerCopyResolverArgs {
  id: number;
  authorId: number;
}

export class BannerCopyResolver implements IResolver<IBannerCopyResolverArgs, { banner: IBannerFull}> {
  public name = 'bannerCopy';
  public description = 'copy banner';
  public type = new GraphQLNonNull(bannerFullEnvelopedType);

  public args = {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    authorId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  };

  public resolve = this.authorization.check<IBannerCopyResolverArgs>(
    this.authorization.security.permission.createAnyBanner, // tfsAdmin
    async (
      _source: ISource,
      args: IBannerCopyResolverArgs,
      context: IContext,
      _info: GraphQLResolveInfo,
    ): Promise<{ banner: IBannerFull} | Error> => {
      try {
        const copiedBanner = await context.bannerService.copy(args.id, args.authorId);
        context.logger.debug(
          { originBannerId: args.id, copiedBanner },
          'Banner was copied.',
        );

        return { banner: copiedBanner };
      } catch (e) {
        return context.errorHandling(e);
      }
    },
  );

  public constructor(private authorization: IAuthorizationComponent) {}
}
