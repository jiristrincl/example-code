import { IBannerFullArrayEnveloped, bannerFullArrayEnveloped } from '../../../../entities';
import { IContext, IResolver, ISource, IAuthorizationComponent } from '../../../../infrastructure';

import {
  GraphQLList, GraphQLNonNull, GraphQLResolveInfo, GraphQLString,
} from 'graphql';

export class BannerGetBySubtypesResolver implements IResolver<{ subTypes: string[] }, IBannerFullArrayEnveloped> {
  public name = 'bannerGetBySubtypes';
  public description = 'get banners by subTypes';
  public type = new GraphQLNonNull(bannerFullArrayEnveloped);

  public args = {
    subTypes: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLString)),
    },
  };

  public resolve = this.authorization.check<{ subTypes: string[] }>(
    this.authorization.security.permission.readOwnBanner, // user
    async (
      _source: ISource,
      args: { subTypes: string[] },
      context: IContext,
      _info: GraphQLResolveInfo,
    ): Promise<IBannerFullArrayEnveloped | Error> => {
      try {
        const bannersResponse: IBannerFullArrayEnveloped = { banners: await context.bannerService.getBySubtypes(args.subTypes) };

        return bannersResponse;
      } catch (e) {
        return context.errorHandling(e);
      }
    },
  );

  public constructor(private authorization: IAuthorizationComponent) {}
}
