import { container, MockFactory2TestResolvers } from '../container';
import { IBannerButtonGateway, IBannerGateway, IBannerSubtypeGateway } from '../data-providers';
import {
  IBannerButtonWithId, IBannerCore, IBannerCreate, IBannerWithId, IBannerSubTypeWithId, IBannerSoftUpdate, IBannerButton, IFileUrls,
} from '../entities';

import { IFileService } from './fileService';

import { BannerService, IBannerService } from '.';

import { expect } from 'chai';

describe('Unit bannerService', () => {
  let subTypes: string[];
  let bannerId: number;
  let bannerCore: IBannerCore;
  let bannerCreate: IBannerCreate;
  let button: IBannerButtonWithId;
  let bannerSubTypes: IBannerSubTypeWithId[];
  let fileUrls: IFileUrls;
  let runner: any;

  let mockFileService: IFileService;
  let mockBannerGateway: IBannerGateway;
  let mockBannerButtonGateway: IBannerButtonGateway;
  let mockBannerSubtypeGateway: IBannerSubtypeGateway;
  let mockFactory: MockFactory2TestResolvers;

  let initService: (bannerGw: IBannerGateway) => IBannerService;
  let bannerService: IBannerService;

  before(() => {
    mockFactory = MockFactory2TestResolvers.getInstance(container);

    bannerId = 1;
    subTypes = ['axia', 'prisma'];
    bannerCore = mockFactory.banner;
    bannerCreate = {
      ...bannerCore,
      subTypes,
      button: {
        isEnabled: true,
        text: 'click here!!!',
        link: 'epic link',
      },
    };
    button = {
      ...bannerCreate.button!,
      id: 2,
      bannerId,
    };
    bannerSubTypes = [
      {
        id: 11,
        bannerId,
        subType: bannerCreate.subTypes![0],
      },
      {
        id: 12,
        bannerId,
        subType: bannerCreate.subTypes![1],
      },
    ];
    fileUrls = {
      fileUrl: 'https://url',
      signedUrl: 'https://signedUrl',
      key: 'key',
    };

    mockFileService = {
      getUrlsForBanner: async (): Promise<IFileUrls> => fileUrls,
      getUrlsForCampaign: async (): Promise<IFileUrls> => fileUrls,
      getUrlsForCatalogue: async (): Promise<IFileUrls> => fileUrls,
      getUrlsForProduct: async (): Promise<IFileUrls> => fileUrls,
      getUrlsForAvatar: async (): Promise<IFileUrls> => fileUrls,
      getUrlsForOrder: async (): Promise<IFileUrls> => fileUrls,
      getUrlsForTests: async (): Promise<IFileUrls> => fileUrls,
      copyFile: async (): Promise<void> => {

      },
      deleteFile: async (): Promise<void> => {

      },
    };

    mockBannerButtonGateway = {
      save: async (buttonSave: IBannerButton): Promise<IBannerButtonWithId | undefined> => ({
        ...buttonSave,
        id: button.id,
      }),
      updateByBannerId: async (buttonUpdate: IBannerButton): Promise<IBannerButtonWithId | undefined> => ({
        ...buttonUpdate,
        id: button.id,
      }),
      deleteAllWhereBannerId: async (): Promise<void> => { },
      getById: async (): Promise<IBannerButtonWithId | undefined> => button,
      getByBannerId: async (): Promise<IBannerButtonWithId | undefined> => button,
    };

    mockBannerSubtypeGateway = {
      saveMultiple: async (
        bannerId: number, subTypes: string[],
      ): Promise<IBannerSubTypeWithId[]> => subTypes.map((subType: string, index: number) => ({
        id: index,
        bannerId,
        subType,
      })),
      deleteAllWhereBannerId: async (bannerId: number): Promise<void> => {
        runner = bannerId;
      },
      getByBannerId: async (): Promise<IBannerSubTypeWithId[]> => bannerSubTypes,
      getBySubType: async (): Promise<IBannerSubTypeWithId[]> => bannerSubTypes,
    };

    initService = (bannerGw: IBannerGateway): IBannerService => new BannerService(
      mockFileService,
      bannerGw,
      mockBannerButtonGateway,
      mockBannerSubtypeGateway,
      mockFactory.mockContainer.utils.logger,
      mockFactory.mockContainer.utils.prometheus,
    );
  });

  beforeEach(() => {
    mockBannerGateway = {
      save: async (banner: IBannerCore): Promise<IBannerWithId | undefined> => ({
        ...banner,
        id: bannerId,
      }),
      update: async (banner: IBannerWithId): Promise< IBannerWithId | undefined> => ({
        ...banner,
        publicationDate: new Date('2020-02-02').toISOString(),
      }),
      softUpdate: async (banner: IBannerSoftUpdate): Promise<IBannerWithId | undefined> => {
        runner = banner;
        return {
          ...bannerCore,
          id: bannerId,
        };
      },
      delete: async (): Promise<void> => { },
      softDelete: async (id: number): Promise<IBannerWithId | undefined> => {
        runner = id;
        return {
          ...bannerCore,
          id: bannerId,
        };
      },
      getById: async (id: number): Promise<IBannerWithId | undefined> => ({
        ...bannerCore,
        id,
        publicationDate: new Date('2020-02-02').toISOString(),
      }),
      getAll: async (): Promise<IBannerWithId[]> => [{
        ...bannerCore,
        id: bannerId,
      }],
      getByIdArray: async (): Promise<IBannerWithId[]> => [{
        ...bannerCore,
        id: bannerId,
      }],

    };
    bannerService = initService(mockBannerGateway);
  });

  it('.create()', async () => {
    const response = await bannerService.create(bannerCreate);
    expect(response).to.be.deep.equal({
      ...bannerCreate,
      id: bannerId,
      button,
    });
  });

  it('.create() - throw Error (missing subtypes)', async () => {
    let response: any;

    try {
      response = await bannerService.create({
        ...bannerCreate,
        isDraft: false,
        subTypes: [],
      });
    } catch (error) {
      response = error;
    }
    expect(response.message).to.contains('Published banner must have subtypes');
  });

  it('.create() - throw Error (missing duration)', async () => {
    let response: any;

    try {
      response = await bannerService.create({
        ...bannerCreate,
        isDraft: false,
        durationFrom: undefined,
      });
    } catch (error) {
      response = error;
    }
    expect(response.message).to.contains('Published banner must have duration');
  });

  it('.create() - throw Error (missing button link)', async () => {
    let response: any;

    try {
      response = await bannerService.create({
        ...bannerCreate,
        button: {
          ...bannerCreate.button!,
          isEnabled: true,
          link: undefined,
        },
      });
    } catch (error) {
      response = error;
    }
    expect(response.message).to.contains('Enabled button must have link and text');
  });

  it('.update()', async () => {
    const response = await bannerService.update({
      ...bannerCreate,
      id: bannerId,
      button,
    });

    expect(runner).to.be.deep.equal(bannerId);
    expect(response).to.be.deep.equal({
      ...bannerCreate,
      id: bannerId,
      publicationDate: new Date('2020-02-02').toISOString(),
      button,
    });
  });

  it('.softUpdate() - softDelete', async () => {
    await bannerService.softUpdate({
      isDeleted: true,
      id: bannerId,
    });

    expect(runner).to.be.deep.equal(bannerId);
  });

  it('.softUpdate() - recover', async () => {
    await bannerService.softUpdate({
      isDeleted: false,
      id: bannerId,
    });

    expect(runner).to.be.deep.equal({
      isDeleted: false,
      id: bannerId,
    });
  });

  it('.softUpdate() - priority', async () => {
    await bannerService.softUpdate({
      priority: 2,
      id: bannerId,
    });

    expect(runner).to.be.deep.equal({
      priority: 2,
      id: bannerId,
    });
  });

  it('.softUpdate() - draft to publish', async () => {
    await bannerService.softUpdate({
      isDraft: false,
      id: bannerId,
    });

    expect(runner.isDraft).to.be.deep.equal(false);
    expect(runner.publicationDate).to.be.a('string');
  });

  it('.softUpdate() - draft to publish - fails', async () => {
    mockBannerGateway = {
      ...mockBannerGateway,
      getById: async (): Promise<IBannerWithId | undefined> => ({
        ...bannerCore,
        heading: undefined,
        id: bannerId,
      }),
    };
    bannerService = initService(mockBannerGateway);

    let response: any;
    try {
      await bannerService.softUpdate({
        isDraft: false,
        id: bannerId,
      });
    } catch (e) {
      response = e;
    }

    expect(response.message).to.contains('Published banner must have heading');
  });

  it('.copy()', async () => {
    const response = await bannerService.copy(1, 666);

    expect(response.name).to.be.deep.equal(`${bannerCore.name} - COPY`);
    expect(response.publicationDate).to.be.deep.equal(undefined);
    expect(response.isDraft).to.be.deep.equal(true);
    expect(response.subTypes).to.be.deep.equal(subTypes);
    expect(response.authorId).to.be.deep.equal(666);
    expect(response.button).to.be.deep.equal(button);
  });

  it('.delete()', async () => {
    const response = await bannerService.delete(1);
    expect(response).to.be.deep.equal(undefined);
  });

  it('.getAll()', async () => {
    const response = await bannerService.getAll();
    expect(response).to.be.deep.equal([{
      ...bannerCore,
      id: bannerId,
    }]);
  });

  it('.getById()', async () => {
    const response = await bannerService.getById(bannerId);
    expect(response).to.be.deep.equal({
      ...bannerCreate,
      id: bannerId,
      publicationDate: new Date('2020-02-02').toISOString(),
      button,
    });
  });

  it('.getBySubtypes()', async () => {
    const response = await bannerService.getBySubtypes(subTypes);
    expect(response).to.be.deep.equal([{
      ...bannerCore,
      id: bannerId,
      button,
    }]);
  });

  it('.getSignedUrl()', async () => {
    const response = await bannerService.getSignedUrl('suffix');
    expect(response).to.be.deep.equal(fileUrls);
  });
});
