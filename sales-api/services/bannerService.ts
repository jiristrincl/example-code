import { IFileService } from './fileService';
import { IBannerButtonGateway, IBannerGateway, IBannerSubtypeGateway } from '../data-providers';

import {
  IBannerButton,
  IBannerButtonCore,
  IBannerCreate, IBannerFull, IBannerSoftUpdate, IBannerSubTypeWithId, IBannerWithId, IFileUrls,
} from '../entities';

import {
  Prometheus, MissingParameterError, NotFoundError, Logger, InternalServerError,
} from 'tfs-npm-utils';

export interface IBannerService {
  create(banner: IBannerCreate): Promise<IBannerFull>;
  update(banner: IBannerFull): Promise<IBannerFull>;
  softUpdate(banner: IBannerSoftUpdate): Promise<IBannerWithId[]>;
  copy(bannerId: number, authorId: number): Promise<IBannerFull>;
  delete(id: number): Promise<void>;
  getById(id: number): Promise<IBannerFull>;
  getAll(): Promise<IBannerWithId[]>;
  getBySubtypes(subtypes: string[]): Promise<IBannerFull[]>;
  getSignedUrl(suffix: string): Promise<IFileUrls>;
}

enum MethodsList {
  create = 'create',
  update = 'update',
  softUpdate = 'softUpdate',
  copy = 'copy',
  delete = 'delete',
  getById = 'getById',
  getAll = 'getAll',
  getBySubtypes = 'getBySubtypes',
  getSignedUrl = 'getSignedUrl',
}

export class BannerService implements IBannerService {
  public constructor(
    private fileService: IFileService,
    private bannerGateway: IBannerGateway,
    private bannerButtonGateway: IBannerButtonGateway,
    private bannerSubtypeGateway: IBannerSubtypeGateway,
    private logger: Logger,
    private prometheus: Prometheus,
  ) {}

  public create = async (banner: IBannerCreate): Promise<IBannerFull> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.create });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.create });

    const bannerCreate = banner;

    if (!banner.isDraft) {
      this.validatePublishedBanner(banner);
      bannerCreate.publicationDate = new Date().toISOString();
    }

    this.validateBannerButton(banner.button);

    const bannerSaved = await this.bannerGateway.save(bannerCreate);
    if (!bannerSaved) {
      throw new InternalServerError('Banner could not be saved.', { bannerName: banner.name });
    }

    const button = await this.bannerButtonGateway.save({
      ...banner.button,
      bannerId: bannerSaved.id,
    });

    if (!button) {
      throw new Error('Banner button could not be saved.');
    }

    let subTypes: IBannerSubTypeWithId[] = [];
    if (banner.subTypes && banner.subTypes.length > 0) {
      subTypes = await this.bannerSubtypeGateway.saveMultiple(bannerSaved.id, banner.subTypes);
      if (subTypes.length !== banner.subTypes.length) {
        throw new InternalServerError('There was same problem of saving bannerSubtype couple', { bannerId: bannerSaved.id });
      }
    }

    const bannerFull: IBannerFull = {
      ...bannerSaved,
      subTypes: subTypes.map((subType: IBannerSubTypeWithId) => subType.subType),
      button,
    };

    this.logger.debug(
      { bannerFull },
      'Banner was created',
    );
    end?.call(0);
    return bannerFull;
  };

  public update = async (banner: IBannerFull): Promise<IBannerFull> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.update });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.update });

    const bannerCreate = banner;

    if (!banner.isDraft) {
      this.validatePublishedBanner(banner);
      bannerCreate.publicationDate = new Date().toISOString();
    }

    this.validateBannerButton(banner.button);

    const oldBanner = await this.bannerGateway.getById(banner.id);
    if (!oldBanner) {
      throw new InternalServerError('There is no banner with id', { bannerId: banner.id });
    }
    if (oldBanner.image && banner.image !== oldBanner.image) {
      await this.fileService.deleteFile(oldBanner.image);
    }

    const bannerUpdated = await this.bannerGateway.update(bannerCreate);
    if (!bannerUpdated) {
      throw new InternalServerError('Banner could not be updated.', { bannerId: banner.id });
    }

    const button = await this.bannerButtonGateway.updateByBannerId({
      ...banner.button,
      bannerId: banner.id,
    });

    if (!button) {
      throw new Error('Banner button could not be updated.');
    }

    let subTypes: IBannerSubTypeWithId[] = [];
    if (banner.subTypes && banner.subTypes.length > 0) {
      await this.bannerSubtypeGateway.deleteAllWhereBannerId(banner.id);
      subTypes = await this.bannerSubtypeGateway.saveMultiple(banner.id, banner.subTypes);
      if (subTypes.length !== banner.subTypes.length) {
        throw new InternalServerError('There was same problem of saving bannerSubtype couple', { bannerId: banner.id });
      }
    }

    const bannerFull: IBannerFull = {
      ...bannerUpdated,
      subTypes: subTypes.map((subType: IBannerSubTypeWithId) => subType.subType),
      button,
    };

    this.logger.debug(
      { bannerFull },
      'Banner was created',
    );
    end?.call(0);
    return bannerFull;
  };

  public softUpdate = async (banner: IBannerSoftUpdate): Promise<IBannerWithId[]> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.softUpdate });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.softUpdate });

    const bannerForUpdate: IBannerSoftUpdate = banner;

    if (banner.isDraft === false) {
      const bannerFromDb = await this.getById(banner.id);
      this.validatePublishedBanner(bannerFromDb);
      bannerForUpdate.publicationDate = new Date().toISOString();
    } else if (banner.isDraft === true) {
      bannerForUpdate.publicationDate = null;
    }

    let bannerFull: IBannerWithId | undefined;
    if (banner.isDeleted === true) {
      bannerFull = await this.bannerGateway.softDelete(bannerForUpdate.id);
    } else {
      bannerFull = await this.bannerGateway.softUpdate(bannerForUpdate);
    }

    if (!bannerFull) {
      throw new InternalServerError('Banner could not be updated.', { bannerId: banner.id });
    }

    const banners = await this.bannerGateway.getAll();

    end?.call(0);
    return banners;
  };

  public copy = async (bannerId: number, authorId: number): Promise<IBannerFull> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.copy });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.copy });

    const originalBanner: IBannerFull = await this.getById(bannerId);

    let urls: IFileUrls | undefined;
    if (originalBanner.image) {
      const urlSplitted = originalBanner.image.split('.');
      urls = await this.getSignedUrl(urlSplitted[urlSplitted.length - 1]);
      await this.fileService.copyFile(urls.fileUrl, originalBanner.image);
    }

    const bannerCopy: IBannerCreate = {
      ...originalBanner,
      name: `${originalBanner.name} - COPY`,
      priority: 0,
      isDraft: true,
      authorId,
      publicationDate: undefined,
      image: urls ? urls.fileUrl : undefined,
      button: {
        ...originalBanner.button,
      },
    };

    const bannerFull: IBannerFull = await this.create(bannerCopy);

    this.logger.debug(
      {
        originBannerId: bannerId,
        bannerCopy: bannerFull,
      },
      'Banner was copied',
    );
    end?.call(0);
    return bannerFull;
  };

  public delete = async (id: number): Promise<void> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.delete });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.delete });

    await Promise.all([
      await this.bannerSubtypeGateway.deleteAllWhereBannerId(id),
      await this.bannerButtonGateway.deleteAllWhereBannerId(id),
    ]);

    const savedBanner = await this.bannerGateway.getById(id, true);
    if (!savedBanner) {
      throw new InternalServerError('There is no banner with given id', { id });
    }
    if (savedBanner.image) {
      await this.fileService.deleteFile(savedBanner.image);
    }

    await this.bannerGateway.delete(id);

    this.logger.debug({ id }, 'Banner was deleted');
    end?.call(0);
  };

  public getById = async (id: number): Promise<IBannerFull> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.getById });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.getById });

    const banner = await this.bannerGateway.getById(id);
    if (!banner) {
      throw new NotFoundError('Banner was not found', { bannerId: id });
    }
    const button = await this.bannerButtonGateway.getByBannerId(banner.id);
    if (!button) {
      throw new NotFoundError('Button for given banner was not found', { bannerId: id });
    }
    const subTypes = await this.bannerSubtypeGateway.getByBannerId(banner.id);

    const bannerFull: IBannerFull = {
      ...banner,
      button,
      subTypes: subTypes.map((subType: IBannerSubTypeWithId) => subType.subType),
    };

    end?.call(0);
    return bannerFull;
  };

  public getAll = async (): Promise<IBannerWithId[]> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.getAll });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.getAll });

    const banners = await this.bannerGateway.getAll();

    end?.call(0);
    return banners;
  };

  public getBySubtypes = async (subtypes: string[]): Promise<IBannerFull[]> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.getBySubtypes });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.getBySubtypes });

    let bannerIds: number[] = [];
    await Promise.all(
      subtypes.map(async (subType: string) => {
        const couples = await this.bannerSubtypeGateway.getBySubType(subType);
        bannerIds = [
          ...bannerIds,
          ...couples.map((couple: IBannerSubTypeWithId) => couple.bannerId),
        ];
      }),
    );

    const uniqueBannerIds: number[] = bannerIds.filter(
      (
        id: number,
        index: number,
        array: number[],
      ) => array.indexOf(id) === index,
    );
    const bannersWithId = await this.bannerGateway.getByIdArray(uniqueBannerIds);

    const bannersWithButton: IBannerFull[] = await Promise.all(
      bannersWithId.map(async (bannerWithId: IBannerWithId) => {
        const button = await this.bannerButtonGateway.getByBannerId(bannerWithId.id);
        if (!button) {
          throw new Error('Banner button could not be saved.');
        }

        return {
          ...bannerWithId,
          button,
        };
      }),
    );

    end?.call(0);
    return bannersWithButton;
  };

  public getSignedUrl = async (suffix: string): Promise<IFileUrls> => {
    this.prometheus.useCase.MessagesCount?.inc({ name: MethodsList.getSignedUrl });
    const end = this.prometheus.useCase.MessagesTime?.startTimer({ name: MethodsList.getSignedUrl });

    const urls = await this.fileService.getUrlsForBanner(suffix);
    end?.call(0);
    return urls;
  };

  private validatePublishedBanner = (banner: IBannerCreate | IBannerFull): void => {
    if (!banner.subTypes || !banner.subTypes.length) {
      throw new MissingParameterError('Published banner must have subtypes');
    }
    if (!banner.durationFrom) {
      throw new MissingParameterError('Published banner must have duration');
    }
    if (!banner.heading) {
      throw new MissingParameterError('Published banner must have heading');
    }
  };

  private validateBannerButton = (button: IBannerButtonCore | IBannerButton): void => {
    if (button.isEnabled) {
      if (!button.link || !button.text) {
        throw new MissingParameterError('Enabled button must have link and text');
      }
    }
  }
}
